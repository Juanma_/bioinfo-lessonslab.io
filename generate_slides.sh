#run this from the parent directory to the repos
jupyter nbconvert $1/lesson.ipynb --to slides --output index
jupyter nbconvert $1/lesson.ipynb --to html --output notebook
mkdir -p bioinfo-lessons.gitlab.io/public/$1
mkdir -p $1/html
cp $1/index.slides.html $1/html/slides.html
mv $1/index.slides.html bioinfo-lessons.gitlab.io/public/$1/index.html
cp $1/notebook.html $1/html/
mv $1/notebook.html bioinfo-lessons.gitlab.io/public/$1/notebook.html
cp -r $1/img bioinfo-lessons.gitlab.io/public/$1/
